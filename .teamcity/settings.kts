import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2019.2"

project {
//    buildType(BuildTest)
//    buildType(BuildAccept)
    buildType(BuildProd)
}


open class BuildBase(val pipelineBuild: String, val enviroment: String, val enviromentShort: String) : BuildType({
    name = pipelineBuild

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "Compile source code"
            scriptContent = "rm -rf build && npm i --target_arch=x64 --target_platform=linux --allow-root && npm run build"
        }
        script {
            name = "Run envly-seed"
            workingDir = "/home/pi/envly-seed"
            scriptContent = "./seed.sh"
        }
        script {
            name = "Add .env values"
            scriptContent = "envly deploy ${DslContext.projectName} ${enviroment}"
        }
        script {
            name = "Copy .env to build/.env"
            scriptContent = "rm -rf build/.env && cp .env build/.env"
        }
        script {
            name = "Zip source code up"
            scriptContent = "zip -r build.zip ."
        }
        script {
            name = "Upload source code to S3 bucket for $enviroment"
            scriptContent = "AWS_PROFILE=$enviroment aws s3 cp build.zip s3://${DslContext.projectName}-${enviromentShort}-bucket/build.zip"
        }
        script {
            name = "Update lambda code version for $enviroment"
            scriptContent = "AWS_PROFILE=$enviroment aws lambda update-function-code --function-name=${DslContext.projectName} --s3-bucket=${DslContext.projectName}-${enviromentShort}-bucket --s3-key=build.zip --region=eu-west-1"
        }
        script {
            name = "Cleanup"
            scriptContent = "rm -rf build.zip && rm -rf build"
        }
    }

    triggers {
        vcs {
        }
    }
})

object BuildTest : BuildBase("BuildTest", "test", "t")
object BuildAccept : BuildBase("BuildAccept", "accept", "a")
object BuildProd : BuildBase("BuildProd", "prod", "p")
