IF exist "C:\Program Files\MySQL\MySQL Server 8.0\data" (
    cd "C:\Program Files\MySQL\MySQL Server 8.0\bin\"
    .\mysqld.exe --console
    EXIT
)
echo "You need to initialize your MySQL install (i.e running .\mysqld.exe --initialize)"
