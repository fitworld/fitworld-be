###Requirements
Remember to disable auto startup for mysql server.
* Mysql Workbench&Server
* Redis (included within this repo)

### .env setup
Check out the .env.example OR

SSH into pi ```ssh pi@p.i``` or similiar, then do `cat ~/envly-seed/seed.sh`

### How to
for mysql

````
(elevated terminal)
.\start-mysql.bat
````

for redis
````
Open up WSL (Ubuntu linux from Windows)
    => cd <into this directory>
    => ./redis-server
````

### How to connect to RDS and Elasticache
```
Teamcity (p.i:8111 or similiar) 
    => fitworld-be-terraform project
        => latest build
            => check outputs for commands to run
                => run commands in seperate terminals and keep both alive
                   as long as the proxy is needed  
```
