import { Injectable } from '@nestjs/common';
import createMollieClient, {MollieClient} from "@mollie/api-client";


@Injectable()
export class MollieService {
    private readonly _client: MollieClient;

    constructor() {
        this._client = createMollieClient({
            apiKey: String(process.env.MOLLIE_API_KEY)
        })
    }

    getClient(): MollieClient {
        return this._client;
    }
}
