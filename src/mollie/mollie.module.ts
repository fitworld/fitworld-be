import { Module } from '@nestjs/common';
import { MollieService } from './mollie.service';
import {PaymentService} from "../graphql/entity-services/payment/payment.service";
import {PaymentModule} from "../graphql/entity-services/payment/payment.module";
import { MollieController } from './mollie.controller';

@Module({
  imports: [PaymentModule],
  providers: [MollieService],
  exports: [MollieService],
  controllers: [MollieController]
})
export class MollieModule {}
