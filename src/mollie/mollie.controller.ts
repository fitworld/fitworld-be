import {Body, Controller, Injectable, Post} from '@nestjs/common';
import {Auth} from "../authentication/guards/decoratos/authentication-type.decorator";
import {AuthenticationType} from "../authentication/guards/AuthenticationType";
import {ApiTags} from "@nestjs/swagger";
import Tags from "../swagger/tags";
import {WebhookDto} from "./dto/webhook.dto";
import {MollieService} from "./mollie.service";
import {PaymentService} from "../graphql/entity-services/payment/payment.service";
import {PaymentStatus} from "../database/models/Payment";
import {List, Refund} from "@mollie/api-client";

@Injectable()
@Controller('mollie')
export class MollieController {
    constructor(
        private readonly mollieService: MollieService,
        private readonly paymentService: PaymentService,
    ) {
    }

    @Post("/")
    @Auth([AuthenticationType.OPEN], [])
    @ApiTags(Tags.Mollie)
    async paymentWebhook(@Body() body: WebhookDto) {
        const res = await this.mollieService.getClient().payments.get(body.id);
        const payment = await this.paymentService.findOneByOpts({
            where: {
                mollieId: body.id
            }
        });
        if (payment) {
            let status = res.status;
            console.log(`PAYMENT (FROM DB) STATUS: `, payment.status);
            console.log(`PAYMENT (FROM MOLLIE) STATUS: `, res.status);
            console.log(`MOLLIE RESOURCE: `, res.resource);

            let refunds;
            const getRefunds = async (from: string | undefined): Promise<Function | Refund> => {
                refunds = await this.mollieService.getClient().payments_refunds.page({
                    paymentId: body.id,
                    from
                });
                if (refunds.nextPageCursor) return await getRefunds(refunds[refunds.length - 1].id);
                else return refunds[refunds.length - 1];
            };
            const lastRefund = await getRefunds(undefined) as Refund;
            console.log(`LAST REFUND: `, lastRefund);
            if (!!lastRefund) status = lastRefund.status;

            // weird mollie typings
            payment.status = status as unknown as PaymentStatus;
            await payment.save();
        }
    }
}
