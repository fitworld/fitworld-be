// Swagger annotations are generated on-the-fly with Nest CLI
import {IsEmail, IsNotEmpty} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

// TODO add something like address or more personal info etc
// TODO add relation to if the trainer belongs to a company or is a freelancer
// TODO make those relation one-way

export class WebhookDto {
    @IsNotEmpty()
    @ApiProperty({
        name: "id",
        required: true,
        type: String,
        description: "mollie payment ID to check if status is valid",
        example: "tr_d0b0E3EA3v"
    })
    id!: string;
}
