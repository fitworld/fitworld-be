import {UserType} from "../../database/models/User";

export interface IJWTToken {
    id: string;
    userType: UserType
    salt: string;
    iat: string;
    exp: string;
    iss: 'fitworld.io'
}
