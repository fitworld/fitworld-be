import {IJWTToken} from "./IJWTToken";
import {Request} from "express";
import {User} from "../../database/models/User";

export interface IAuthenticatedRequest extends Request {
    token: IJWTToken | undefined;
    user: User | undefined;
}
