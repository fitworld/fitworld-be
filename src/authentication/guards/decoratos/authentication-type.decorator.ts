import {SetMetadata} from '@nestjs/common';
import {AuthenticationType} from "../AuthenticationType";
import {Role} from "../../../database/models/User";

export interface IAuthenticationTypeRoles {
    authenticationTypes: AuthenticationType[],
    roles: Role[]
}

export const Auth = (authenticationTypes: AuthenticationType[], roles: Role[]) =>
    SetMetadata('authenticationType.roles', <IAuthenticationTypeRoles>{
            authenticationTypes,
            roles
        }
    );
