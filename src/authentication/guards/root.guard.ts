import {
    BadRequestException,
    CanActivate,
    ExecutionContext,
    Injectable,
    InternalServerErrorException,
    UnauthorizedException
} from '@nestjs/common';
import {RedisService} from "../../redis/redis.service";
import {AuthenticationService} from "../authentication.service";
import {IAuthenticatedRequest} from "../interfaces/IAuthenticatedRequest";
import {Reflector} from "@nestjs/core";
import {AuthenticationType} from "./AuthenticationType";
import {Role} from "../../database/models/User";
import {IAuthenticationTypeRoles} from "./decoratos/authentication-type.decorator";
import {GqlExecutionContext} from "@nestjs/graphql";

@Injectable()
export class RootGuard implements CanActivate {
    private _authenticationTypes: AuthenticationType[] | null = null;
    private _roles: Role[] | null = null;


    constructor(
        private readonly authenticationService: AuthenticationService,
        private readonly redisService: RedisService,
        private readonly reflector: Reflector
    ) {
    }

    requireAnon = (val: AuthenticationType): boolean => val === AuthenticationType.ANONYMOUS_REQUIRED;
    requireUser = (val: AuthenticationType): boolean => val === AuthenticationType.FULL_USER_REQUIRED;
    requireAny = (val: AuthenticationType): boolean => val === AuthenticationType.ANONYMOUS_REQUIRED || val === AuthenticationType.FULL_USER_REQUIRED;
    optionalAnon = (val: AuthenticationType): boolean => val === AuthenticationType.ANONYMOUS_OPTIONAL;
    optionalUser = (val: AuthenticationType): boolean => val === AuthenticationType.FULL_USER_OPTIONAL;
    open = (val: AuthenticationType): boolean => val === AuthenticationType.OPEN;

    isGiven = (predicate: (el: AuthenticationType) => boolean) => this._authenticationTypes?.find(predicate) != null;

    async canActivate(
        context: ExecutionContext,
    ): Promise<boolean> {
        const obj = this.reflector.get<IAuthenticationTypeRoles>('authenticationType.roles', context.getHandler()) as IAuthenticationTypeRoles ?? {};
        this._authenticationTypes = obj.authenticationTypes;
        this._roles = obj.roles;

        if (this._authenticationTypes == null) throw new InternalServerErrorException("Route has no Auth definition, denying all requests for safety.");
        if (this._roles == null) throw new InternalServerErrorException("Route has no Role definition, denying all requests for safety.");

        if (this.isGiven(this.open)) return true;

        // incase of REST
        let request = context.switchToHttp().getRequest() as IAuthenticatedRequest;

        // incase of GraphQL (query/mutation, excluding subscriptions)
        if (request == null) {
            const ctx = GqlExecutionContext.create(context);
            request = ctx.getContext().req;
        }
        // Authorization: Bearer eywkdwadji29d2jakwjidwj => ['Bearer', 'eywkdwadji29d2jakwjidwj']
        const v = request.header('Authorization')?.trimLeft().trimRight().split(' ') ?? [];
        const t = v[1];

        if (this.isGiven(this.requireAny)) {
            if (v[0] !== "Bearer")
                throw new BadRequestException("Invalid Bearer token format (did you use Authorization: Bearer <TOKEN>) correctly?");
            if (!t) throw new UnauthorizedException("bearer signature found, but no token");
        }

        const token = await this.authenticationService.verifyToken(t);

        if (this.isGiven(this.requireAny) && token == null) throw new UnauthorizedException("Invalid token");
        if (!!token) {
            // check roles before authenticating too
            const user = await this.authenticationService.getUser(token.id);
            if (!user && this.isGiven(this.requireAny)) throw new InternalServerErrorException("jwt is valid and signed from server but somehow the user does not exist associated with the given jwt");

            if (this._roles.length > 0 && !this._roles.includes(user?.role ?? Role.None) && this.isGiven(this.requireAny)) {
                throw new UnauthorizedException("invalid permissions (roles) for this endpoint");
            }
            request.token = token;
            request.user = user;
        }

        return true;
    }
}
