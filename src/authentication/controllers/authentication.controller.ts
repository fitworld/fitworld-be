import {
    BadRequestException,
    Body,
    Controller,
    Get,
    InternalServerErrorException,
    Next,
    NotFoundException,
    Post,
    Query,
    Req,
    Res,
    UnauthorizedException
} from '@nestjs/common';
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import bcrypt from "bcrypt";
import Tags from "../../swagger/tags";
import {
    AuthenticationService,
    PassportGoogleAuthenticateParams,
    PassportGoogleCallbackParams
} from "../authentication.service";
import {NextFunction, Response} from "express";
import {authenticate} from "passport";
import {OAuthFlowTypes} from "../dto/HandleOAuthRequest.dto";
import {RedisService} from "../../redis/redis.service";
import {InjectRepository} from "@nestjs/typeorm";
import {Role, User} from "../../database/models/User";
import {Repository} from "typeorm";
import {MakeAnonymousUserDto} from "../dto/MakeAnonymousUser.dto";
import {Auth} from "../guards/decoratos/authentication-type.decorator";
import axios from "axios";
import {AuthenticationType} from "../guards/AuthenticationType";
import {IAuthenticatedRequest} from "../interfaces/IAuthenticatedRequest";
import {UsrPwdDto} from "../dto/UsrPwd.dto";
import {MakeTrainerRegisterDto} from "../dto/MakeTrainerRegister.dto";
import {LoginTrainerDto} from "../dto/LoginTrainer.dto";
import {UserService} from "../../graphql/entity-services/user/user.service";

export interface TokenResponse {
    token: string
}

@Controller({path: '/auth'})
export class AuthenticationController {
    constructor(
        private readonly authenticationService: AuthenticationService,
        private readonly redisService: RedisService,
        private readonly userService: UserService,
        @InjectRepository(User)
        private readonly usersRepository: Repository<User>
    ) {
    }

    @Post('trainer/register')
    @Auth([AuthenticationType.OPEN], [])
    @ApiTags(Tags.AuthenticationTrainer)
    async makeTrainer(@Body() data: MakeTrainerRegisterDto) {
        const trainer = await this.authenticationService.makeTrainer(data);
        return <TokenResponse>{
            token: await this.authenticationService.createNewToken(trainer.id, trainer.type)
        }
    }

    @Post('trainer/login')
    @Auth([AuthenticationType.OPEN], [])
    @ApiTags(Tags.AuthenticationTrainer)
    async loginTrainer(@Body() data: LoginTrainerDto, @Res() res: Response) {
        const trainer = await this.userService.getUser(data.email, [Role.Trainer]);
        if (!trainer) throw new NotFoundException("bad password or email");
        // password and email match, authenticate
        if (bcrypt.compareSync(data.password, trainer.password)) {
            const token = await this.authenticationService.createNewToken(trainer.id, trainer.type);
            return res.status(200)
                .json({
                    token
                });
        } else {
            throw new UnauthorizedException("bad password or email");
        }
    }

    // Flow 1: Make 'anonymous' user when opening app for the first time.
    @Post('anon')
    @Auth([AuthenticationType.OPEN], [])
    @ApiTags(Tags.Authentication)
    @ApiOperation({
        summary: "Use this endpoint to create an anonymous user. String can be random within bounds, except at position 1, 3 and 5 it all must be a number between 4-6",
    })
    async makeAnonymousUser(@Body() data: MakeAnonymousUserDto): Promise<TokenResponse> {
        const user = await this.authenticationService.makeAnonymousUser();
        return <TokenResponse>{
            token: await this.authenticationService.createNewToken(user.id, user.type)
        };
    }


    @Get('me')
    @Auth([AuthenticationType.ANONYMOUS_REQUIRED, AuthenticationType.FULL_USER_REQUIRED], Object.keys(Role).map(k => Role[k as any]) as unknown as Role[])
    @ApiTags(Tags.AuthenticationGoogle)
    async me(@Req() req: IAuthenticatedRequest) {
        const user = await this.usersRepository.findOne({id: req.token?.id});
        if (!user) throw new NotFoundException("can't find user associated with this jwt token");
        const {password, ...rest} = user;
        return {
            ...rest
        };
    }

    @Post('usrpwd/register')
    @Auth([AuthenticationType.ANONYMOUS_OPTIONAL], [])
    @ApiTags(Tags.Authentication)
    async usrpwdRegister(
        @Body() body: UsrPwdDto,
        @Req() req: IAuthenticatedRequest,
        @Res() res: Response,
        @Next() next: NextFunction,
    ) {
        let user;
        if (!!req.token) {
            user = await this.usersRepository.findOne({id: req.token.id});
            if (!user) throw new NotFoundException("user associated with this user was not found");
            if (user.role >= Role.Anonymous) throw new BadRequestException("that user (email or password) is already a full user or higher");
            const fullUser = await this.userService.getUser(user.email, [Role.User]);
            if (fullUser) throw new BadRequestException("can't upgrade user because there's already a user registered with that email");

            user = await this.authenticationService.upgradeAnonymousUser(body.email, body.password, user);
        } else {
            user = await this.authenticationService.makeFullUserFromUsrPwd(body.email, body.password);
        }
        const token = await this.authenticationService.createNewToken(user.id, user.type);

        return res.status(200)
            .json({
                token
            });
    }

    @Post('usrpwd/login')
    @Auth([AuthenticationType.OPEN], [])
    @ApiTags(Tags.Authentication)
    async usrpwdLogin(
        @Body() body: UsrPwdDto,
        @Req() req: IAuthenticatedRequest,
        @Res() res: Response,
        @Next() next: NextFunction,
    ) {
        const user = await this.userService.getUser(body.email, [Role.User]);
        if (!user) throw new NotFoundException("bad password or email");
        // password and email match, authenticate
        if (bcrypt.compareSync(body.password, user.password)) {
            const token = await this.authenticationService.createNewToken(user.id, user.type);
            return res.status(200)
                .json({
                    token
                });
        } else {
            throw new UnauthorizedException("bad password or email");
        }
    }

    // flow for RN
    @Get('done')
    @Auth([AuthenticationType.ANONYMOUS_OPTIONAL], [])
    @ApiTags(Tags.AuthenticationGoogle)
    async done(@Res() res: Response) {
        res.sendStatus(200);
    }

    // Flows for google (and by extension email/pw)
    @Get('google')
    @Auth([AuthenticationType.ANONYMOUS_OPTIONAL], [])
    @ApiTags(Tags.AuthenticationGoogle)
    async handleOAuthRequest(
        @Query("auth_flow") auth_flow: OAuthFlowTypes,
        @Query("referrer") referrer: string,
        @Req() req: IAuthenticatedRequest,
        @Res() res: Response,
        @Next() next: NextFunction,
    ) {
        if (auth_flow == null || auth_flow.length === 0 || !(auth_flow in OAuthFlowTypes)) throw new BadRequestException("bad auth_flow parameter");

        let params = <PassportGoogleAuthenticateParams>{
            session: false,
            scope: ["https://www.googleapis.com/auth/plus.me", "https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"],
            callbackURL: `/auth/google/callback?auth_flow=${auth_flow}`,
        };

        params = await this.authenticationService.constructGoogleCallbackURL(params, auth_flow, referrer, req) as PassportGoogleAuthenticateParams;

        // use passport purely for redirect/authorization code handling because lazy
        authenticate('google', params)(req, res, next);
    }

    // Flow 2: Make account based from google login
    @Get('google/callback')
    @Auth([AuthenticationType.ANONYMOUS_OPTIONAL], [])
    @ApiTags(Tags.AuthenticationGoogle)
    async handleOAuthCallback(
        @Query("auth_flow") auth_flow: OAuthFlowTypes,
        @Query("user_id") user_id: string,
        @Query("referrer") referrer: string,
        @Req() req: IAuthenticatedRequest,
        @Res() res: Response,
        @Next() next: NextFunction
    ): Promise<TokenResponse | any> {
        if (auth_flow == null || auth_flow.length === 0 || !(auth_flow in OAuthFlowTypes)) throw new BadRequestException("bad auth_flow parameter");

        let params = <PassportGoogleCallbackParams>{
            session: false,
            state: req.query.state,
            callbackURL: `/auth/google/callback?auth_flow=${auth_flow}`
        };

        params = await this.authenticationService.constructGoogleCallbackURL(params, auth_flow, referrer, req);

        authenticate('google', params, async (err, googleProfile: GoogleProfile.Profile) => {
            if (err) return new UnauthorizedException("received error from google:", err);
            if (!googleProfile) throw new UnauthorizedException("didn't receive user back from google");

            const {data} = await axios.get<GoogleProfile.OAuthProfile>(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${googleProfile.access_token}`);
            if (data.email == googleProfile._json.email) {
                let user = await this.usersRepository.findOne({email: data.email});
                if (!!user) {
                    const token = await this.authenticationService.createNewToken(user.id, user.type);
                    if (referrer) {
                        res.redirect(`https://${referrer}?token=${token}`);
                    } else {
                        res.redirect(`/auth/done?token=${token}`);
                    }
                } else {
                    // not found by id .. maybe it's an anonymous user, we should check
                    user = await this.usersRepository.findOne({id: user_id});
                    if (!!user) {
                        // found an anonymous user, we should now upgrade the anonymous user to a full user.
                        const usr = await this.authenticationService.upgradeAnonymousUser(googleProfile._json.email, undefined, user);
                        if (!usr) throw new InternalServerErrorException("couldn't upgrade anonymous user to full user");
                        const token = await this.authenticationService.createNewToken(usr.id, usr.type);
                        if (referrer) {
                            res.redirect(`http://${referrer}?token=${token}`);
                        } else {
                            res.redirect(`/auth/done?token=${token}`);
                        }

                    } else {
                        const usr = await this.authenticationService.makeFullUserFromGoogle(googleProfile);
                        if (!usr) throw new InternalServerErrorException("couldn't make a new user from fresh google login");
                        const token = await this.authenticationService.createNewToken(usr.id, usr.type);
                        if (referrer) {
                            res.redirect(`https://${referrer}?token=${token}`);
                        } else {
                            res.redirect(`/auth/done?token=${token}`);
                        }
                        // we can't find anything, so we should make a new user by email etc
                    }
                }
            } else {
                throw new UnauthorizedException("mismatch in emails that was given and the email associated with the access token from google.");
            }
        })(req, res, next);
    }
}
