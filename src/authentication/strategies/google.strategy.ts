import {PassportStrategy} from "@nestjs/passport";
import * as express from "express-serve-static-core";
import {Strategy} from "passport-google-oauth20";
import {AuthenticationService} from "../authentication.service";

export class GoogleStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authenticationService: AuthenticationService) {
        super(
            {
                clientID: process.env.GOOGLE_CLIENT_ID,
                clientSecret: process.env.GOOGLE_CLIENT_SECRET,
                callbackURL: "/authentication/google/callback",
                passReqToCallback: true
            }, (req: express.Request, access_token: string, refresh_token: string, profile: Omit<GoogleProfile.Profile, 'access_token'>, done: (p1: any, p2?: any) => null) => {
                return done(null, <GoogleProfile.Profile>{
                    ...profile,
                    access_token
                });
            }
        );
    }
}
