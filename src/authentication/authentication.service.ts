import {BadRequestException, Injectable, InternalServerErrorException} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Role, User, UserType} from "../database/models/User";
import {Repository} from "typeorm";
import jwt from "jsonwebtoken";
import {IJWTToken} from "./interfaces/IJWTToken";
import bcrypt from "bcrypt";
import {RedisService} from "../redis/redis.service";
import {OAuthFlowTypes} from "./dto/HandleOAuthRequest.dto";
import {IAuthenticatedRequest} from "./interfaces/IAuthenticatedRequest";
import uuid from "uuid/v4";
import {MakeTrainerRegisterDto} from "./dto/MakeTrainerRegister.dto";
import {UserService} from "../graphql/entity-services/user/user.service";

export interface PassportGoogleCallbackParams {
    session: boolean,
    state?: any,
    callbackURL: string
}

export interface PassportGoogleAuthenticateParams extends PassportGoogleCallbackParams {
    scope: string[]
}

export interface SignTokenInput {
    id: string,
    userType: UserType,
    salt: string
}

@Injectable()
export class AuthenticationService {
    constructor(
        @InjectRepository(User)
        private readonly usersRepository: Repository<User>,
        private readonly usersService: UserService,
        private readonly redisService: RedisService,
    ) {
    }

    async saltPassword (pwd: string): Promise<string> {
        return bcrypt.hashSync(pwd, 10);
    }

    async makeTrainer(data: MakeTrainerRegisterDto): Promise<User> {
        if (await this.usersService.getUser(data.email, [Role.Trainer])) throw new BadRequestException("user already exists");

        const trainer = await this.usersRepository.create({
            type: UserType.User,
            role: Role.Trainer,
            email: data.email,
            password: await this.saltPassword(data.password)
        });
        await this.usersRepository.save(trainer);
        return trainer;
    }

    async makeFullUserFromGoogle(googleProfile: GoogleProfile.Profile): Promise<User> {
        if (await this.usersService.getUser(googleProfile._json.email, [Role.Anonymous, Role.User])) throw new BadRequestException("user already exists");

        const user = await this.usersRepository.create({
            type: UserType.User,
            email: googleProfile._json.email,
            role: Role.User
        });
        await this.usersRepository.save(user);
        return user;
    }

    async makeFullUserFromUsrPwd(email: string, password: string): Promise<User> {
        if (await this.usersService.getUser(email, [Role.User, Role.Anonymous])) throw new BadRequestException("user already exists");
        const user = await this.usersRepository.create({
            type: UserType.User,
            email: email,
            password: await this.saltPassword(password),
            role: Role.User
        });
        await this.usersRepository.save(user);
        return user;
    }

    async makeAnonymousUser(): Promise<User> {
        const user = this.usersRepository.create({
            type: UserType.Anonymous
        });
        await this.usersRepository.save(user);
        return user;
    }


    async upgradeAnonymousUser(email: string, password: string | undefined, user: User): Promise<User> {
        if (!user) throw new InternalServerErrorException("couldn't find user to upgrade from anonymous");
        user.type = UserType.User;
        user.email = email;
        if (!!password) user.password = await this.saltPassword(password);
        if (user.role <= Role.Anonymous) {
            user.role = Role.User;
        }
        await this.usersRepository.save(user);
        return user;
    }

    async addTokenToUser(id: string, token: string): Promise<void> {
        // check if alreay exists in redis
        if ((await this.checkIfTokenAlreadyExists(id, token))) {
            // this should NEVER happen
            throw new InternalServerErrorException("token already is already known.");
        }
        await this.redisService.hset(id, token, token);
    }

    async checkIfTokenAlreadyExists(id: string, token: string) {
        return !!(await this.redisService.hget(id, token));
    }

    // Hack because we can't inject the User repository in the root guard, which is needed
    async getUser(id: string) {
        return await this.usersRepository.findOne({id})
    }

    async createNewToken(id: string, type: UserType): Promise<string> {
        const token = await this.signToken(<SignTokenInput>{
            id: id,
            userType: type,
            salt: uuid()
        });
        await this.addTokenToUser(id, token);
        return token;
    }

    async signToken(value: SignTokenInput): Promise<string> {
        return jwt.sign(value, process.env.JWT_SECRET as string, {
            expiresIn: '1000y',
            algorithm: "HS256",
            issuer: "fitworld.io"
        });
    }

    async verifyToken(value: string | undefined): Promise<IJWTToken | null> {
        let token: IJWTToken | null;
        try {
            // TODO check in redis if token exists
            token = jwt.verify(value ?? "thiswillfail", process.env.JWT_SECRET as string) as unknown as IJWTToken;
            return token;
        } catch (e) {
            // TODO think more about this
            // JWT throws an "error" if the token is invalid, not so handy if we're doing optional tokens or tokens that might fail
        }
        return null;
    }

    async constructGoogleCallbackURL(params: PassportGoogleCallbackParams | PassportGoogleAuthenticateParams, auth_flow: string, referrer: string, req: IAuthenticatedRequest): Promise<PassportGoogleCallbackParams | PassportGoogleAuthenticateParams> {
        switch (auth_flow) {
            // expect nothing, just handle as usual
            case OAuthFlowTypes.NORMAL:
                break;

            // expect valid JWT in header that corresponds to a user
            case OAuthFlowTypes.ANONYMOUS_UPGRADE:
                if (req.token == null) throw new BadRequestException("couldn't find a user with that token while requesting an anonymous_upgrade.");
                params.callbackURL = `${params.callbackURL}&user_id=${req.token.id}`;
                break;
            default:
                throw new BadRequestException("couldn't understand oauthflow type");
        }
        if (!!referrer) {
            params.callbackURL = `${params.callbackURL}&referrer=${referrer}`
        }
        return params;
    }
}
