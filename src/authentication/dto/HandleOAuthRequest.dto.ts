export enum OAuthFlowTypes {
    NORMAL = "NORMAL",
    ANONYMOUS_UPGRADE = "ANONYMOUS_UPGRADE",
}
