// Swagger annotations are generated on-the-fly with Nest CLI
import {IsEmail, IsNotEmpty} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class UsrPwdDto {
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({
        name: "email",
        required: true,
        type: String,
        example: "thomas@thomas.nl"
    })
    email!: string;

    @IsNotEmpty()
    @ApiProperty({
        name: "password",
        required: true,
        type: String,
        example: "helloworld"
    })
    password!: string;
}
