declare module GoogleProfile {

    export interface Name {
        familyName: string;
        givenName: string;
    }

    export interface Email {
        value: string;
        verified: boolean;
    }

    export interface Photo {
        value: string;
    }

    export interface Json {
        sub: string;
        name: string;
        given_name: string;
        family_name: string;
        picture: string;
        email: string;
        email_verified: boolean;
        locale: string;
    }

    export interface OAuthProfile {
        id: string;
        email: string;
        verified_email: boolean;
        name: string;
        given_name: string;
        family_name: string;
        picture: string;
        locale: string;
    }

    export interface Profile {
        id: string;
        displayName: string;
        name: Name;
        emails: Email[];
        photos: Photo[];
        access_token: string;
        provider: string;
        _raw: string;
        _json: Json;
    }
}
