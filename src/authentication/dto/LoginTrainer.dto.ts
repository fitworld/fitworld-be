// Swagger annotations are generated on-the-fly with Nest CLI
import {IsEmail, IsNotEmpty} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

// TODO add something like address or more personal info etc
// TODO add relation to if the trainer belongs to a company or is a freelancer
// TODO make those relation one-way

export class LoginTrainerDto {
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({
        name: "email",
        required: true,
        type: String,
        example: "thomas@thomas.nl"
    })
    email!: string;

    @IsNotEmpty()
    @ApiProperty({
        name: "password",
        required: true,
        type: String,
        example: "helloworld"
    })
    password!: string;
}
