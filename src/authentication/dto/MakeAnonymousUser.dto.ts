import {IsNotEmpty, MaxLength, MinLength} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {MakeAnonymousUserMagicStringRule} from "../../rules/MakeAnonymousUserMagicString.rule";

// Swagger annotations are generated on-the-fly with Nest CLI
export class MakeAnonymousUserDto {
    @IsNotEmpty()
    @MinLength(50)
    @MaxLength(200)
    @MakeAnonymousUserMagicStringRule()
    @ApiProperty({
        name: "magicString",
        required: true,
        type: String,
        example: "h5o4a6ow92kmm1JWiodwjidWJDioJWIODJWIODJOAKLSJDKWHDKWAwjd92dj282"
    })
    magicString!: string;
}
