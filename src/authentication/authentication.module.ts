import {Module} from '@nestjs/common';
import {AuthenticationController} from './controllers/authentication.controller';
import {AuthenticationService} from './authentication.service';
import {RedisModule} from "../redis/redis.module";
import {User} from "../database/models/User";
import {TypeOrmModule} from "@nestjs/typeorm";
import {PassportModule} from "@nestjs/passport";
import {GoogleStrategy} from "./strategies/google.strategy";
import {APP_GUARD} from "@nestjs/core";
import {RootGuard} from "./guards/root.guard";
import {UserModule} from "../graphql/entity-services/user/user.module";

@Module({
    imports: [
        TypeOrmModule.forFeature([User]),
        PassportModule,
        UserModule,
        RedisModule,
    ],
    controllers: [AuthenticationController],
    providers: [
        {
            provide: APP_GUARD,
            useClass: RootGuard,
        },
        AuthenticationService,
        GoogleStrategy,
    ],
    exports: [AuthenticationService]
})
export class AuthenticationModule {
}
