// These should be on top of the import list (reflection/.env parsing)
require("dotenv").config();
import "reflect-metadata"


import {APIGatewayProxyEvent, APIGatewayProxyResult, Context} from "aws-lambda";
import {Server} from "http";
import awsServerlessExpress from "aws-serverless-express";
import bootstrapNest from "./bootstrap-nest";
import bootstrapExpress from "./bootstrap-express";

let env = process.env.ENV;
let hotServer: Server;

const bootstrap = async () => {
    const expressApp = await bootstrapExpress();
    return {
        nestApp: await bootstrapNest(expressApp),
        expressApp: expressApp
    }
};

if (env === "dev") {
    bootstrap()
        .then(async (apps) => {
            await apps.nestApp.listen(4000);
            console.log("server ready at http://localhost:4000");
        })
        .catch(err => console.log(err));
} else {
    exports.handler = async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
        if (!hotServer) {
            const apps = await bootstrap();
            hotServer = awsServerlessExpress.createServer(apps.expressApp);
            return awsServerlessExpress.proxy(hotServer, event, context, 'PROMISE').promise;
        } else {
            return awsServerlessExpress.proxy(hotServer, event, context, 'PROMISE').promise;
        }
    };
}


