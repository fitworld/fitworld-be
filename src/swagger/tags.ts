enum Tags {
    Authentication = "Authentication",
    AuthenticationGoogle = "Authentication Google",
    AuthenticationTrainer = "Authentication Trainer",
    Mollie = "Mollie",
    Core = "Core",
}

export default Tags;
