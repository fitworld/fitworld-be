import {Injectable} from '@nestjs/common';
import {Repository} from "typeorm";
import {Session} from "../../../database/models/Session";
import {InjectRepository} from "@nestjs/typeorm";
import {GenericService} from "../GenericService";
import {PaginationOpts} from "../../sharedTypes";
import {Location} from "../../../database/models/Location";
import {FindAllSessionReturnType} from "../../resolvers/session/session.resolver";

@Injectable()
export class SessionService extends GenericService<Session> {
    static readonly degreeToMeterRatio = {
        decimalDegree: 0.00001,
        meter: 1.1132
    };

    constructor(
        @InjectRepository(Session)
        private readonly sessionRepository: Repository<Session>
    ) {
        super(sessionRepository);
    }


    findAllSessions = async (start: Date, end: Date, latitude: number, longitude: number, rangeInMeters: number, paginationOpts: PaginationOpts) => {
        const absLat = Math.abs(latitude);
        const absLon = Math.abs(longitude);
        const range = (rangeInMeters / SessionService.degreeToMeterRatio.meter) * SessionService.degreeToMeterRatio.decimalDegree;

        return (await Promise.all((await this.sessionRepository.createQueryBuilder("session")
            .innerJoin("session.location", "location")
            .innerJoin("location.coordinates", "coordinates")
            .where("ABS(coordinates.latitude) BETWEEN :minLat AND :maxLat", {
                minLat: absLat - range,
                maxLat: absLat + range
            })
            .andWhere("ABS(coordinates.longitude) BETWEEN :minLon AND :maxLon", {
                minLon: absLon - range,
                maxLon: absLon + range
            })
            .andWhere("session.time BETWEEN :minTime AND :maxTime", {minTime: start, maxTime: end})
            .skip(paginationOpts.page * paginationOpts.pageSize)
            .take(paginationOpts.pageSize)
            .getMany())
            .map(async (sess): Promise<[Session, FindAllSessionReturnType]> => {
                const l = (await this.relation(sess.id, ["location", "location.coordinates"])) as unknown as Location[];
                const location = l[0];
                let d = new FindAllSessionReturnType();
                if (!!location) {
                    const c = (a: number, b: number) => Math.abs(Math.abs(a) - b);
                    const latDist = c(location.coordinates.latitude as number, absLat);
                    const lonDist = c(location.coordinates.longitude as number, absLon);

                    const x = ((latDist ** 2) + (lonDist ** 2));
                    const y = Math.sqrt(x);

                    d.distanceInMeters = (y / SessionService.degreeToMeterRatio.decimalDegree) * SessionService.degreeToMeterRatio.meter;
                }

                console.log(sess);
                return [sess, d];
            })))

    };

    search = async (input: string, {pageSize, page}: PaginationOpts) => {
        return await this.sessionRepository.find({
            take: pageSize,
            skip: page * pageSize,
            join: {alias: 'session', innerJoin: {training: 'session.training'}},
            where: (qb: any) => {
                qb.where({
                    a: 1,
                    b: 2
                }).andWhere('training.name = :name', {name: input});
            }
        })
    }
}
