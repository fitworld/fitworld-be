import { Module } from '@nestjs/common';
import { LocationService } from './location.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Location} from "../../../database/models/Location";

@Module({
  imports: [
    TypeOrmModule.forFeature([Location])
  ],
  providers: [LocationService],
  exports: [LocationService]
})
export class LocationModule {}
