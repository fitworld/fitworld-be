import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Location} from "../../../database/models/Location";
import {GenericService} from "../GenericService";

@Injectable()
export class LocationService extends GenericService<Location> {
    constructor(
        @InjectRepository(Location)
        private readonly locationRepository: Repository<Location>
    ) {
        super(locationRepository);
    }
}
