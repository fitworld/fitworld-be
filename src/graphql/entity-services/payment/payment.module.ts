import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Payment} from "../../../database/models/Payment";

@Module({
  imports: [
      TypeOrmModule.forFeature([Payment])
  ],
  providers: [PaymentService],
  exports: [PaymentService]
})
export class PaymentModule {}
