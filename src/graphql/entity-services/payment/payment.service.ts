import { Injectable } from '@nestjs/common';
import {GenericService} from "../GenericService";
import {Payment} from "../../../database/models/Payment";
import {Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class PaymentService extends GenericService<Payment> {
    constructor(
        @InjectRepository(Payment)
        private readonly paymentRepository: Repository<Payment>
    ) {
        super(paymentRepository);
    }
}
