import { Module } from '@nestjs/common';
import { CompanyService } from './company.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Location} from "../../../database/models/Location";
import {Company} from "../../../database/models/Company";

@Module({
  imports: [
    TypeOrmModule.forFeature([Company])
  ],
  providers: [CompanyService],
  exports: [CompanyService]
})
export class CompanyModule {}
