import { Injectable } from '@nestjs/common';
import {GenericService} from "../GenericService";
import {Company} from "../../../database/models/Company";
import {InjectRepository} from "@nestjs/typeorm";
import {User} from "../../../database/models/User";
import {Repository} from "typeorm";

@Injectable()
export class CompanyService extends GenericService<Company> {
    constructor(
        @InjectRepository(Company)
        private readonly companyRepository: Repository<Company>
    ) {
        super(companyRepository);
    }
}
