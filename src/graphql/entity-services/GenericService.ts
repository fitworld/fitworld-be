import {DeepPartial, FindManyOptions, ObjectLiteral, Repository} from "typeorm";
import {Payment} from "../../database/models/Payment";
import {OverrideBaseEntity} from "../../database/models/OverrideBaseEntity";

export class GenericService<T1 extends OverrideBaseEntity> {
    private _repository: Repository<T1>;

    constructor(repository: Repository<T1>) {
        this._repository = repository;
        this.relation.bind(this);
    }

    async create(entity: T1) {
        const e = this._repository.create(entity as unknown as DeepPartial<T1>);
        return await ((e as T1).save())
    }


    findOneByOpts = async (opts: FindManyOptions<T1>) => {
        return await this._repository.findOne(opts);
    };

    findById = async (id: string, relations: string[] = []) => {
        return await this._repository.findOne({
            relations,
            where: {
                id,
            }
        })
    };

    async relation<T1>(id: string, relation: string): Promise<T1 | undefined>;
    async relation<T1>(id: string, relations: string[]): Promise<any[] | undefined>;
    async relation<T1>(id: string, relations: string | string[]): Promise<T1 | any[] | undefined> {
        const e = (await this._repository.findOne({
                relations: Array.isArray(relations) ? relations : [relations],
                where: {
                    id,
                }
            }
        )) as ObjectLiteral;

        if (Array.isArray(relations)) {
            // could be any array of any relations (entities) being loaded in
            return relations.map(x => e[x] as any);
        } else if (!!e && e[relations]) {
            return e[relations];
        }
        return undefined;
    }
}
