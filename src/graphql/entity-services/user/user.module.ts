import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Location} from "../../../database/models/Location";
import {User} from "../../../database/models/User";

@Module({
  imports: [
    TypeOrmModule.forFeature([User])
  ],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
