import { Injectable } from '@nestjs/common';
import {GenericService} from "../GenericService";
import {Role, User} from "../../../database/models/User";
import {InjectRepository} from "@nestjs/typeorm";
import {Training} from "../../../database/models/Training";
import {Repository} from "typeorm";

@Injectable()
export class UserService extends GenericService<User> {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>
    ) {
        super(userRepository);
    }

    async findOne(email: string): Promise<User | undefined> {
        return this.userRepository.findOne({
            email: email
        })
    }

    async getUser(email: string, roles: Role[]) {
        const usr = await this.userRepository.find({email});
        const filtered = usr.filter(x => x.email === email && roles.includes(x.role));
        if (filtered.length === 1) {
            return filtered[0];
        }
    }

}
