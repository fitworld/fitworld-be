import { Injectable } from '@nestjs/common';
import {GenericService} from "../GenericService";
import {Image} from "../../../database/models/Image";
import {InjectRepository} from "@nestjs/typeorm";
import {User} from "../../../database/models/User";
import {Repository} from "typeorm";

@Injectable()
export class ImageService extends GenericService<Image> {
    constructor(
        @InjectRepository(Image)
        private readonly imageRepository: Repository<Image>
    ) {
        super(imageRepository);
    }
}
