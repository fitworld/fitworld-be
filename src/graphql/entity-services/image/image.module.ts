import { Module } from '@nestjs/common';
import { ImageService } from './image.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Location} from "../../../database/models/Location";
import {Image} from "../../../database/models/Image";

@Module({
  imports: [
    TypeOrmModule.forFeature([Image])
  ],
  providers: [ImageService],
  exports: [ImageService]
})
export class ImageModule {}
