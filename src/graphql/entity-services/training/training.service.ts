import { Injectable } from '@nestjs/common';
import {GenericService} from "../GenericService";
import {Training} from "../../../database/models/Training";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

@Injectable()
export class TrainingService extends GenericService<Training> {
    constructor(
        @InjectRepository(Training)
        private readonly trainingRepository: Repository<Training>
    ) {
        super(trainingRepository);
    }
}
