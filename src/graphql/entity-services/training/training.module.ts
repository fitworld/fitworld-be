import {Module} from '@nestjs/common';
import {TrainingService} from './training.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Training} from "../../../database/models/Training";

@Module({
    imports: [
        TypeOrmModule.forFeature([Training])
    ],
    providers: [TrainingService],
    exports: [TrainingService]
})
export class TrainingModule {
}
