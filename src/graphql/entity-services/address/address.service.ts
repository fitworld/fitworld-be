import { Injectable } from '@nestjs/common';
import {GenericService} from "../GenericService";
import {Address} from "../../../database/models/Address";
import {InjectRepository} from "@nestjs/typeorm";
import {User} from "../../../database/models/User";
import {Repository} from "typeorm";

@Injectable()
export class AddressService extends GenericService<Address> {
    constructor(
        @InjectRepository(Address)
        private readonly addressRepository: Repository<Address>
    ) {
        super(addressRepository);
    }
}
