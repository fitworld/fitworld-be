import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Location} from "../../../database/models/Location";
import {Address} from "../../../database/models/Address";

@Module({
  imports: [
    TypeOrmModule.forFeature([Address])
  ],
  providers: [AddressService],
  exports: [AddressService]
})
export class AddressModule {}
