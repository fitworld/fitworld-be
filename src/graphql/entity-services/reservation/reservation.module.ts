import {Module} from '@nestjs/common';
import {ReservationService} from './reservation.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Reservation} from "../../../database/models/Reservation";
import {User} from "../../../database/models/User";
import {UserService} from "../user/user.service";
import {UserModule} from "../user/user.module";

@Module({
    imports: [
        UserModule,
        TypeOrmModule.forFeature([Reservation, User])
    ],
    providers: [ReservationService],
    exports: [ReservationService]
})
export class ReservationModule {
}
