import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Reservation} from "../../../database/models/Reservation";
import {QueryBuilder, Repository} from "typeorm";
import {GenericService} from "../GenericService";
import {PaginationOpts} from "../../sharedTypes";
import {User} from '../../../database/models/User';
import {UserService} from "../user/user.service";

@Injectable()
export class ReservationService extends GenericService<Reservation> {
    constructor(
        @InjectRepository(Reservation)
        private readonly reservationRepository: Repository<Reservation>,
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly userService: UserService
    ) {
        super(reservationRepository);
    }

    myReservations = async (userId: string) => {
        const user = await this.userRepository.findOne({
            relations: ["payments", "payments.reservation"],
            where: {
                id: userId
            },
        });
        if (!user) throw new Error("could not find the current logged in user by id somehow");
        return user.payments.map(x => x.reservation)
    }
}
