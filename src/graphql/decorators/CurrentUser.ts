import {createParamDecorator, ExecutionContext} from "@nestjs/common";
import {GqlExecutionContext} from "@nestjs/graphql";

export const CurrentUser = createParamDecorator(
    (data: unknown, ctx: any) => {
        let user = null;
        // hack but ctx is weird object, appareantly fixed in nestjs 7 but we can't upgrade due to outputtype issues of @nestjs/graphql in v7
        for(const o of ctx) {
            if (!!o) {
                if (o.req) {
                    user = o.req.user;
                }
            }
        }
        return user;
    }
);
