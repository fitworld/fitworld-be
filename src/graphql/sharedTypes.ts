import {Field, InputType, ObjectType} from "type-graphql";

@ObjectType()
export class ErrorResponse {
    @Field(type => String)
    message!: string;
    @Field(type => String, {nullable: true})
    stacktrace!: string;
}

@ObjectType()
export class ResponseStatusReturn {
    @Field(type => Boolean)
    status!: boolean;
    @Field(type => [ErrorResponse], {nullable: "items"})
    errors!: ErrorResponse[];
}

@InputType()
export class PaginationOpts {
    @Field(type => Number)
    page!: number;

    @Field(type => Number)
    pageSize!: number;
}
