import {Module} from '@nestjs/common';
import {GraphqlcoreService} from './graphqlcore.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../database/models/User";
import {Company} from "../database/models/Company";
import {Location} from "../database/models/Location";
import {Reservation} from "../database/models/Reservation";
import {Session} from "../database/models/Session";
import {Training} from "../database/models/Training";
import {ResolversModule} from "./resolvers/resolvers.module";

@Module({
    imports: [
        ResolversModule,
        TypeOrmModule.forFeature([
            User,
            Company,
            Location,
            Reservation,
            Session,
            Training,
        ]),
    ],
    providers: [
        GraphqlcoreService,
    ]
})
export class GraphqlcoreModule {
}
