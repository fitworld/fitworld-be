import { registerEnumType } from "type-graphql";
import {Role, UserType} from "../database/models/User";
import {CurrencyCode, PaymentStatus} from "../database/models/Payment";

// Register enums used in GraphQL here

export default () => {
    registerEnumType(UserType, {
        name: "UserType",
        description: "The type of user.",
    });
    registerEnumType(Role, {
        name: "Role",
        description: "The role of user.",
    });
    registerEnumType(PaymentStatus, {
        name: "PaymentStatus",
        description: "Status of the payment"
    });
    registerEnumType(CurrencyCode, {
        name: "CurrencyCode",
        description: "Currency code, e.g EUR"
    });
}

