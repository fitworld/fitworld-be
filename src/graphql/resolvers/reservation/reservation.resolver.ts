import {Args, Parent, Query, ResolveProperty, Resolver} from '@nestjs/graphql';
import {Reservation} from "../../../database/models/Reservation";
import {ReservationService} from "../../entity-services/reservation/reservation.service";
import {Session} from "../../../database/models/Session";
import {Role, User} from "../../../database/models/User";
import {MollieService} from "../../../mollie/mollie.service";
import {Auth} from "../../../authentication/guards/decoratos/authentication-type.decorator";
import {AuthenticationType} from "../../../authentication/guards/AuthenticationType";
import {Field, ObjectType} from "type-graphql";
import {PaymentService} from "../../entity-services/payment/payment.service";
import {CurrencyCode, Payment, PaymentStatus} from "../../../database/models/Payment";
import {PaymentMethod, Refund} from "@mollie/api-client";
import {CurrentUser} from "../../decorators/CurrentUser";
import {MollieController} from "../../../mollie/mollie.controller";
import {SessionService} from "../../entity-services/session/session.service";
import {PaginationOpts} from "../../sharedTypes";

@ObjectType()
export class PayReservationReturn {
    @Field(type => String)
    checkoutLink!: string;
    @Field(type => Reservation)
    reservation!: Reservation
}

@ObjectType()
export class RefundReservationReturn {
    @Field(type => Payment)
    payment!: Payment;
}

type ExtendedMolliePayment = {
    status: string | null,
    id: string | null,
    _links: {
        checkout: {
            href: string
        }
    }
};

@Resolver(() => Reservation)
export class ReservationResolver {
    constructor(
        private readonly reservationService: ReservationService,
        private readonly paymentService: PaymentService,
        private readonly mollieService: MollieService,
        private readonly sessionService: SessionService,
    ) {
    }

    @Query(returns => PayReservationReturn)
    @Auth([AuthenticationType.ANONYMOUS_REQUIRED, AuthenticationType.FULL_USER_REQUIRED], [Role.Anonymous, Role.User])
    async payReservation(
        @Args("reservationId") reservationId: string,
        @CurrentUser() user: User,
    ): Promise<PayReservationReturn> {
        const reservation = await this.reservationService.findById(reservationId, ["session", "session.training"]);
        if (!reservation) throw new Error("a reservation with that ID does not exist");
        if (!user) throw new Error("user was not attached to req object in request, this should absolutely never happen.");

        const res = (await this.mollieService.getClient().payments.create({
            amount: {
                currency: CurrencyCode.EUR,
                value: reservation.session.price
            },
            description: reservation.session.training.name,
            redirectUrl: "https://fitworld.io/thankyou.html",
            webhookUrl: "https://api.fitworld.io/mollie",
            method: PaymentMethod.ideal,
        })) as unknown as ExtendedMolliePayment;

        const payment = new Payment();
        payment.reservation = reservation;
        payment.status = res.status as PaymentStatus;
        payment.description = `${reservation.session.training.name} on ${reservation.session.time.toDateString()}` ?? "";
        payment.amount = reservation.session.price;
        payment.currencyCode = CurrencyCode.EUR;
        payment.mollieId = res.id ?? "";
        payment.user = user;

        if (!reservation.payments) reservation.payments = [];

        const paymentCreated = await this.paymentService.create(payment);

        reservation.payments.push(
            paymentCreated
        );
        await reservation.save();
        return {
            checkoutLink: res._links.checkout.href,
            reservation
        };
    }

    @Query(returns => RefundReservationReturn)
    @Auth([AuthenticationType.ANONYMOUS_REQUIRED, AuthenticationType.FULL_USER_REQUIRED], [Role.Anonymous, Role.User])
    async refundReservation(
        @Args("paymentId") paymentId: string,
        @CurrentUser() user: User,
    ): Promise<RefundReservationReturn> {
        const payment = await this.paymentService.findById(paymentId, ["user"]);
        if (!payment) throw new Error("a payment with that ID does not exist");
        if (!user) throw new Error("user was not attached to req object in request, this should absolutely never happen.");
        if (payment.status != PaymentStatus.paid) throw new Error("cannot refund non-paid payment.");
        if (payment.user.id != user.id) throw new Error("can't refund for other user, you don't own this payment");

        const res = (await this.mollieService.getClient().payments_refunds.create({
            amount: {
                currency: CurrencyCode.EUR,
                value: payment.amount
            },
            description: `Refund of ${payment.description}`,
            paymentId: payment.mollieId
        })) as unknown as Refund;
        if (res.status == PaymentStatus.pending) {
            payment.status = PaymentStatus.refund_pending;
        } else {
            payment.status = res.status as unknown as PaymentStatus;
        }
        await payment.save();
        return {
            payment
        }
    }

    @Query(returns => [Reservation], {nullable: "items"})
    @Auth([AuthenticationType.ANONYMOUS_REQUIRED, AuthenticationType.FULL_USER_REQUIRED], [Role.Anonymous, Role.User])
    async myReservations(
        @CurrentUser() user: User,
    ): Promise<Reservation[]> {
        return await this.reservationService.myReservations(user.id);
    }

    @ResolveProperty('session', type => Session)
    async training(@Parent() reservation: Reservation) {
        return await this.reservationService.relation<Session>(reservation.id, "session") ?? {};
    }

    @ResolveProperty('payments', type => [Payment])
    async payments(@Parent() reservation: Reservation) {
        return await this.reservationService.relation<Payment>(reservation.id, "payments") ?? [];
    }
}
