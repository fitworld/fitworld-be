import {Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {Location} from "../../../database/models/Location";
import {ImageService} from "../../entity-services/image/image.service";
import {Training} from "../../../database/models/Training";
import {Image} from "../../../database/models/Image";
import {LocationService} from "../../entity-services/location/location.service";
import {Address} from "../../../database/models/Address";
import {Session} from "../../../database/models/Session";
import {Company} from "../../../database/models/Company";
import {Coordinates} from "../../../database/models/Coordinates";

@Resolver(() => Location)
export class LocationResolver {
    constructor(
        private readonly locationService: LocationService,
    ) {
    }

    @ResolveProperty('address', type => Address)
    async training(@Parent() location: Location) {
        return await this.locationService.relation<Address>(location.id, "address") ?? {};
    }

    @ResolveProperty('coordinates', type => Coordinates)
    async coordinates(@Parent() location: Location) {
        return await this.locationService.relation<Coordinates>(location.id, "coordinates") ?? {};
    }

    @ResolveProperty('sessions', type => [Session])
    async sessions(@Parent() location: Location) {
        return await this.locationService.relation<Session>(location.id, "sessions") ?? [];
    }

    @ResolveProperty('companies', type => [Company])
    async companies(@Parent() location: Location) {
        return await this.locationService.relation<Company>(location.id, "companies") ?? [];
    }

}
