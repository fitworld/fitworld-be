import {Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {Training} from "../../../database/models/Training";
import {SessionService} from "../../entity-services/session/session.service";
import {TrainingService} from "../../entity-services/training/training.service";
import {Session} from "../../../database/models/Session";
import {Image} from "../../../database/models/Image";

@Resolver(() => Training)
export class TrainingResolver {
    constructor(
        private readonly trainingService: TrainingService,
    ) {
    }

    @ResolveProperty('images', type => [Image])
    async training(@Parent() training: Training) {
        return await this.trainingService.relation<Image>(training.id, "images") ?? {};
    }

    @ResolveProperty('sessions', type => [Session])
    async sessions(@Parent() training: Training) {
        return await this.trainingService.relation<Session>(training.id, "sessions") ?? {};
    }
}
