import {Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {User} from "../../../database/models/User";
import {Session} from "../../../database/models/Session";
import {Reservation} from "../../../database/models/Reservation";
import {ReservationService} from "../../entity-services/reservation/reservation.service";
import {UserService} from "../../entity-services/user/user.service";
import {Image} from "../../../database/models/Image";

@Resolver(() => User)
export class UserResolver {
    constructor(
        private readonly userService: UserService,
    ) {
    }

    @ResolveProperty('profileImage', type => Image)
    async profileImage(@Parent() user: User) {
        return await this.userService.relation<Image>(user.id, "profileImage") ?? {};
    }

    @ResolveProperty('reservations', type => [Reservation])
    async reservations(@Parent() user: User) {
        return await this.userService.relation<Reservation>(user.id, "reservations") ?? {};
    }

}
