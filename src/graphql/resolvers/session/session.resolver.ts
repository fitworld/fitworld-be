import {Args, Parent, Query, ResolveProperty, Resolver} from '@nestjs/graphql';
import {Auth} from "../../../authentication/guards/decoratos/authentication-type.decorator";
import {AuthenticationType} from "../../../authentication/guards/AuthenticationType";
import {Session} from "../../../database/models/Session";
import {SessionService} from "../../entity-services/session/session.service";
import {Training} from "../../../database/models/Training";
import {Reservation} from "../../../database/models/Reservation";
import {Role} from "../../../database/models/User";
import {Location} from "../../../database/models/Location";
import {PaginationOpts} from "../../sharedTypes";
import {createUnionType, Field, Float, InputType, ObjectType} from "type-graphql";
import {createMollieClient} from '@mollie/api-client';

@InputType()
export class FindAllSessionsCoordinatesInputType {
    @Field(type => Float)
    latitude!: number;
    @Field(type => Float)
    longitude!: number;
}

@ObjectType()
export class FindAllSessionReturnType extends Session {
    @Field(type => Float)
    distanceInMeters!: number;
}


@Resolver(() => Session)
export class SessionResolver {
    constructor(
        private readonly sessionService: SessionService,
    ) {
    }

    @Query(returns => [Session], {nullable: "items"})
    @Auth([AuthenticationType.ANONYMOUS_REQUIRED, AuthenticationType.FULL_USER_REQUIRED], [Role.Anonymous, Role.User])
    async searchSessions(
        @Args("trainingName") trainingName: string,
        @Args("paginationOpts") paginationOpts: PaginationOpts
    ): Promise<Session[]> {
        return await this.sessionService.search(trainingName, paginationOpts);
    }

    @ResolveProperty('training', type => Training)
    async training(@Parent() session: Session) {
        console.log("hi")
        return await this.sessionService.relation<Training>(session.id, "training") ?? {};
    }

    @ResolveProperty('location', type => Location)
    async location(@Parent() session: Session) {
        return await this.sessionService.relation<Location>(session.id, "location") ?? {};
    }

    @ResolveProperty('reservations', type => [Reservation], {nullable: "items"})
    async reservations(@Parent() session: Session) {
        return await this.sessionService.relation<Reservation>(session.id, "reservations") ?? [];
    }
}
