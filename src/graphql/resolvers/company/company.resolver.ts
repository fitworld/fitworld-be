import {Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {Company} from "../../../database/models/Company";
import {LocationService} from "../../entity-services/location/location.service";
import {ImageService} from "../../entity-services/image/image.service";
import {AddressService} from "../../entity-services/address/address.service";
import {Location} from "../../../database/models/Location";
import {Session} from "../../../database/models/Session";
import {CompanyService} from "../../entity-services/company/company.service";
import {Image} from "../../../database/models/Image";
import {Address} from "../../../database/models/Address";

@Resolver(() => Company)
export class CompanyResolver {
    constructor(
        private readonly companyService: CompanyService,
    ) {
    }

    @ResolveProperty('locations', type => [Location])
    async locations(@Parent() company: Company) {
        return await this.companyService.relation<Location>(company.id, "locations") ?? [];
    }

    @ResolveProperty('logo', type => Image)
    async logo(@Parent() company: Company) {
        return await this.companyService.relation<Image>(company.id, "logo") ?? {};
    }

    @ResolveProperty('address', type => Address)
    async address(@Parent() company: Company) {
        return await this.companyService.relation<Address>(company.id, "address") ?? {};
    }

}
