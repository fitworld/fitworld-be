import {Args, Parent, Query, ResolveProperty, Resolver} from '@nestjs/graphql';
import {SessionService} from "../../entity-services/session/session.service";
import {FindAllSessionReturnType, FindAllSessionsCoordinatesInputType} from "../session/session.resolver";
import {Auth} from "../../../authentication/guards/decoratos/authentication-type.decorator";
import {AuthenticationType} from "../../../authentication/guards/AuthenticationType";
import {Role} from "../../../database/models/User";
import {PaginationOpts} from "../../sharedTypes";
import {Training} from "../../../database/models/Training";
import {Session} from "../../../database/models/Session";
import {Location} from "../../../database/models/Location";
import {Reservation} from "../../../database/models/Reservation";

@Resolver(() => FindAllSessionReturnType)
export class SessionDistanceResolver {
    constructor(
        private readonly sessionService: SessionService,
    ) {
    }

    @Query(returns => [FindAllSessionReturnType], {nullable: "items"})
    @Auth([AuthenticationType.ANONYMOUS_REQUIRED, AuthenticationType.FULL_USER_REQUIRED], [Role.Anonymous, Role.User])
    async findAllSessions(
        @Args("start") start: Date,
        @Args("end") end: Date,
        @Args("currentLocation") currentLocation: FindAllSessionsCoordinatesInputType,
        @Args("rangeInMeters") rangeInMeter: number,
        @Args("paginationOpts") paginationOpts: PaginationOpts
    ): Promise<Array<FindAllSessionReturnType>> {
        const a = await this.sessionService.findAllSessions(start, end, currentLocation.latitude, currentLocation.longitude, rangeInMeter, paginationOpts);
        return a.map(x => ({...x[0], ...x[1]})) as unknown as FindAllSessionReturnType[];
    }

    @ResolveProperty('training', type => Training)
    async training(@Parent() session: Session) {
        return await this.sessionService.relation<Training>(session.id, "training") ?? {};
    }

    @ResolveProperty('location', type => Location)
    async location(@Parent() session: Session) {
        return await this.sessionService.relation<Location>(session.id, "location") ?? {};
    }

    @ResolveProperty('reservations', type => [Reservation], {nullable: "items"})
    async reservations(@Parent() session: Session) {
        return await this.sessionService.relation<Reservation>(session.id, "reservations") ?? [];
    }
}
