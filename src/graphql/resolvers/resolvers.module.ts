import {Module} from '@nestjs/common';
import {SessionResolver} from './session/session.resolver';
import {ReservationModule} from "../entity-services/reservation/reservation.module";
import {SessionModule} from "../entity-services/session/session.module";
import {LocationModule} from "../entity-services/location/location.module";
import {TrainingModule} from "../entity-services/training/training.module";
import {UserResolver} from './user/user.resolver';
import {TrainingResolver} from './training/training.resolver';
import {ReservationResolver} from './reservation/reservation.resolver';
import {LocationResolver} from './location/location.resolver';
import {ImageResolver} from './image/image.resolver';
import {CompanyResolver} from './company/company.resolver';
import {AddressResolver} from './address/address.resolver';
import {AddressModule} from "../entity-services/address/address.module";
import {CompanyModule} from "../entity-services/company/company.module";
import {ImageModule} from "../entity-services/image/image.module";
import {UserModule} from "../entity-services/user/user.module";
import {MollieModule} from "../../mollie/mollie.module";
import {PaymentModule} from "../entity-services/payment/payment.module";
import {SessionDistanceResolver} from "./sessionsDistance/sessionDistance.resolver";

// import {SessionDistanceResolver} from "./sessionsDistance/sessionDistance.resolver";

@Module({
    imports: [
        ReservationModule,
        LocationModule,
        TrainingModule,
        SessionModule,
        AddressModule,
        CompanyModule,
        ImageModule,
        PaymentModule,
        UserModule,
        MollieModule
    ],
    providers: [SessionResolver, UserResolver, TrainingResolver, ReservationResolver, LocationResolver, ImageResolver, CompanyResolver, AddressResolver, SessionDistanceResolver]
})
export class ResolversModule {
}
