import {Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {Image} from "../../../database/models/Image";
import {Company} from "../../../database/models/Company";
import {Training} from "../../../database/models/Training";
import {CompanyService} from "../../entity-services/company/company.service";
import {ImageService} from "../../entity-services/image/image.service";

@Resolver(() => Image)
export class ImageResolver {
    constructor(
        private readonly imageService: ImageService,
    ) {
    }

    @ResolveProperty('training', type => Training)
    async training(@Parent() image: Image) {
        return await this.imageService.relation<Training>(image.id, "training") ?? {};
    }
}
