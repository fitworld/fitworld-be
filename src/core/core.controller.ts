import {Controller, Get, Req} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import Tags from "../swagger/tags";
import {Auth} from "../authentication/guards/decoratos/authentication-type.decorator";
import {AuthenticationType} from "../authentication/guards/AuthenticationType";
import {IAuthenticatedRequest} from "../authentication/interfaces/IAuthenticatedRequest";

@Controller()
export class CoreController {

    @Get()
    @Auth([AuthenticationType.OPEN], [])
    @ApiTags(Tags.Core)
    getHello(@Req() req: IAuthenticatedRequest): object {
        return {message: "Hello from Fitworld's BE :)"}
    }
}
