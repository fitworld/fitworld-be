import {Module} from '@nestjs/common';
import {CoreController} from './core.controller';
import {CoreService} from './core.service';
import {GraphQLModule} from '@nestjs/graphql';
import isDev from "../util/isDev";
import {AuthenticationModule} from "../authentication/authentication.module";
import {GraphqlcoreModule} from "../graphql/graphqlcore.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {RedisModule} from "../redis/redis.module";
import {SeederModule} from "../database/seeder/seeder.module";
import {MollieModule} from "../mollie/mollie.module";

@Module({
    imports: [
        GraphQLModule.forRoot({
            context: ({req}) => ({req}),
            debug: isDev(),
            playground: isDev(),
            tracing: true,
            // in-memory instead of a .graphql generated file, might not be great for production
            // with loads of reboots and cold starts (lambda)
            autoSchemaFile: true,
        }),
        TypeOrmModule.forRoot({
            "type": "mysql",
            "host": process.env.RDS_DB_HOST,
            "port": Number(process.env.RDS_DB_PORT),
            "username": process.env.RDS_DB_USERNAME,
            "password": process.env.RDS_DB_PASSWORD,
            "database": "main",
            connectTimeout: 90,
            entities: [
                "build/database/models/**/*.js",
            ],
            migrations: [
                "build/database/migrations/**/*.js"
            ],
            subscribers: [
                "build/database/subscribers/**/*.js"
            ],
            synchronize: isDev(),
            logging: isDev(),

            retryAttempts: 10,
            retryDelay: 5000
        }),
        // Application-level modules after module like graphql/typeorm
        AuthenticationModule,
        RedisModule,
        GraphqlcoreModule,
        SeederModule,
        MollieModule
    ],
    controllers: [CoreController],
    providers: [CoreService],
})
export class CoreModule {
}
