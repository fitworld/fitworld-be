import express from "express";
export default () => {
    // Additional express setup goes here
    const app = express();
    app.disable('x-powered-by');
    return app;
}
