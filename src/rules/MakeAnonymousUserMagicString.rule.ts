import {registerDecorator, ValidationOptions, ValidationArguments} from "class-validator";

export function MakeAnonymousUserMagicStringRule(validationOptions: ValidationOptions = { message: "Are you trying to abuse this endpoint? Seriously?" }) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            propertyName: propertyName,
            name: "MakeAnonymousUserMagicStringRule",
            target: object.constructor,
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const check = (value: string): boolean => Number(value) >= 4 && Number(value) <= 6;
                    return typeof value === "string" &&
                            check(value[1]) &&
                            check(value[3]) &&
                            check(value[5])

                }
            }
        });
    };
}
