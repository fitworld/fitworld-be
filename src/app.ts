// import express from "express";
// import {ApolloServer, gql} from "apollo-server-express";
//
// import anon from "./graphql/anonymous-user/router";
// import swagger from "./swagger/router";
// import onlyDev from "./middleware/onlyDev";
//
// // Construct a schema, using GraphQL schema language
// const typeDefs = gql`
//     type Query {
//         hello: String
//     }
// `;
//
// // Provide resolver functions for your schema fields
// const resolvers = {
//     Query: {
//         hello: () => {
//             return null;
//         },
//     },
// };
//
// const server = new ApolloServer({typeDefs, resolvers});
//
// const app = express();
//
//
// app.use("/anon", anon);
// app.use("/swagger", onlyDev, swagger);
//
// server.applyMiddleware({app});
//
// export default app;
