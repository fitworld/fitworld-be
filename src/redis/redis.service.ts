import {Injectable} from '@nestjs/common';
import redis, {RedisClient} from "redis";
import {promisify} from "util";

// Singleton by default
@Injectable()
export class RedisService {
    client: RedisClient;
    hget: (key: string, field: string) => Promise<any>;
    hset: (hashTableKey: string, fieldName: string, fieldValue: string) => Promise<any>;

    constructor() {
        this.client = redis.createClient({
            host: process.env.REDIS_DB_HOST,
            port: Number(process.env.REDIS_DB_PORT),
            db: Number(process.env.REDIS_DB_NAME),
        });
        this.hset = promisify(this.client.hset).bind(this.client) as unknown as (hashTableKey: string, fieldName: string, fieldValue: string) => Promise<any>;
        this.hget = promisify(this.client.hget).bind(this.client) as unknown as (key: string, field: string) => Promise<any>;
    }
}
