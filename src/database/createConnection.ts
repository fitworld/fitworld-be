import {Connection, createConnection} from "typeorm";

export default async (): Promise<Connection> => {
    return await createConnection({
        "type": "mysql",
        "host": process.env.RDS_DB_HOST,
        "port": 3306,
        "username": process.env.RDS_DB_USERNAME,
        "password": process.env.RDS_DB_PASSWORD,
        "database": "main",
        connectTimeout: 90,
        entities: [
                "build/database/models/**/*.js",
        ],
        migrations: [
                "build/database/migrations/**/*.js"
        ],
        subscribers: [
                "build/database/subscribers/**/*.js"
        ],
        synchronize: true,
        logging: true
    })
}
