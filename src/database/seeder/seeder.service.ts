import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Role, User, UserType} from "../models/User";
import {Company} from "../models/Company";
import {Location} from "../models/Location";
import {Reservation} from "../models/Reservation";
import {Session} from "../models/Session";
import {Training} from "../models/Training";
import _ from "lodash";
import faker from "faker";
import bcrypt from "bcrypt"
import {Image} from "../models/Image";
import {Address} from "../models/Address";
import {Coordinates} from "../models/Coordinates";
import randomLocation, {Point} from "random-location";

type TypeORMPropertiesOnEntities = 'updatedAt' | 'createdAt' | 'hasId' | 'reload' | 'save' | 'remove'

@Injectable()
export class SeederService {
    private static users: User[];
    private static companies: Company[];
    private static locations: Location[];
    private static reservations: Reservation[];
    private static sessions: Session[];
    private static trainings: Training[];

    static readonly n: number = 5;

    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(Company)
        private readonly companyRepository: Repository<Company>,
        @InjectRepository(Location)
        private readonly locationRepository: Repository<Location>,
        @InjectRepository(Reservation)
        private readonly reservationRepository: Repository<Reservation>,
        @InjectRepository(Session)
        private readonly sessionRepository: Repository<Session>,
        @InjectRepository(Training)
        private readonly trainingRepository: Repository<Training>,
        @InjectRepository(Image)
        private readonly imageRepository: Repository<Image>,
        @InjectRepository(Address)
        private readonly addressRepository: Repository<Address>,
        @InjectRepository(Coordinates)
        private readonly coordinatesRepository: Repository<Coordinates>,
    ) {
    }

    private pickValueFromEnum = <T>(origin: { [s: string]: string | number }, isStringEnum = false) => {
        return _
            .sample(
                Object.values(origin)
                    .filter((x) => typeof x !== "string" || isStringEnum)
            ) as unknown as T;
    };

    private generateArray = async <T>(method: () => Promise<Omit<T, TypeORMPropertiesOnEntities>>, amount: number) => Array(5)
        .fill(null)
        .map(async (x) => await method()) as unknown as T[];


    private fakeImage = async (alt: string, url: string): Promise<Image> => {
        const o = new Image();
        o.alt = alt.split(" ")[0];
        o.url = url;
        await this.imageRepository.save(o);
        const image = await this.imageRepository.findOne({
            where: {
                alt: o.alt,
                url: o.url
            }
        });
        if (!image) throw new Error("image could not be created");
        return image;
    };

    private fakeAddress = async (address: string, city: string, province: string, zipcode: string): Promise<Address> => {
        const o = new Address();
        o.address = address;
        o.city = city;
        o.province = province;
        o.zipcode = zipcode;
        await this.addressRepository.save(o);
        const s = await this.addressRepository.findOne({
            where: {
                address: o.address,
                city: o.city,
                province: o.province,
                zipcode: o.zipcode
            }
        });
        if (!s) throw new Error("address could not be created");
        return s;
    };

    private fakeTraining = async (sessions: Session[]): Promise<Training> => {
        const o = new Training();
        o.sessions = sessions;
        o.images = await Promise.all(await Array(_.random(1, SeederService.n))
            .fill(null)
            .map(async (x) =>
                await this.fakeImage(faker.lorem.paragraph(SeederService.n),
                    this.fakeImageUrl(_.random(400, 1920), _.random(400, 1080))
                )));
        o.description = faker.lorem.paragraph(SeederService.n);
        o.name = faker.lorem.word();
        return o;

    };

    private fakeCompany = async (locations: Location[]): Promise<Company> => {
        const o = new Company();
        o.locations = locations;
        o.website = faker.internet.url();
        o.name = faker.company.companyName();
        o.address = await this.fakeAddress(faker.address.streetAddress(), faker.address.city(), faker.address.state(), faker.address.zipCode());
        o.email = faker.internet.email("info", "", o.website);
        o.logo = await this.fakeImage(faker.lorem.words(SeederService.n), this.fakeImageUrl(_.random(400, 1920), _.random(400, 1080)));
        o.telephone_number = faker.phone.phoneNumber();
        return o;
    };

    private fakeLocation = async (sessions: Session[], companies: Company[]): Promise<Location> => {
        const o = new Location();
        o.sessions = sessions;
        o.companies = companies;
        o.address = await this.fakeAddress(faker.address.streetAddress(), faker.address.city(), faker.address.state(), faker.address.zipCode());
        const coords = new Coordinates();
        const point = <Point>{
            latitude: 52.363264,
            longitude: 5.2297728
        };
        coords.longitude = randomLocation.randomCirclePoint(point, 20000).longitude;
        coords.latitude = randomLocation.randomCirclePoint(point, 20000).latitude;
        await this.coordinatesRepository.save(coords);
        o.coordinates = coords;
        return o;
    };

    private fakeSession = async (reservations: Reservation[],
                                 training: Training,
                                 location: Location):
        Promise<Session> => {
        const o = new Session();
        o.reservations = reservations;
        o.location = location;
        o.training = training;
        o.capacity = _.random(1, SeederService.n);
        o.price = String(_.random(1.05, SeederService.n, true).toFixed(2));
        o.time = faker.date.recent(SeederService.n);
        return o;
    };

    private fakeReservation = async (session: Session): Promise<Reservation> => {
        const o = new Reservation();
        o.session = session;
        return o;
    };

    private fakeImageUrl = (width: number, height: number) => {
        return `https://picsum.photos/${width}/${height}`
    };

    private fakeUser = async (reservations: Reservation[]): Promise<User> => {
        const o = new User();
        o.role = this.pickValueFromEnum<Role>(Role);
        o.email = faker.internet.email();
        o.password = await bcrypt.hashSync("moonshine", 10);
        o.type = this.pickValueFromEnum<UserType>(UserType, true);
        o.profileImage = await this.fakeImage(faker.random.words(SeederService.n), this.fakeImageUrl(_.random(400, 1920), _.random(400, 1080)));
        return o;
    };


    private generate = async <T>(amount: number, repository: Repository<T>, factory: () => Promise<T>) => {
        return await Promise.all(Array(amount)
            .fill(null)
            .map(async () => {
                const val = await factory();
                await repository.save(val);
                return val;
            }) as unknown as Promise<T>[]) as unknown as T[];
    };

    async seed() {
        const user = await this.fakeUser([]);
        const training = await this.fakeTraining([]);
        const company = await this.fakeCompany([]);
        const location = await this.fakeLocation([], []);
        const session = await this.fakeSession([], training, location);
        const reservation = await this.fakeReservation(session);

        await this.userRepository.save(user);
        await this.trainingRepository.save(training);
        await this.companyRepository.save(company);
        await this.locationRepository.save(location);
        await this.sessionRepository.save(session);
        await this.reservationRepository.save(reservation);


        SeederService.reservations = await this.generate(_.random(1, SeederService.n), this.reservationRepository, async () => {
            return await this.fakeReservation(session);
        });
        SeederService.users = await this.generate(_.random(1, SeederService.n), this.userRepository, async () => {
            return await this.fakeUser(
                _.sampleSize(SeederService.reservations, _.random(1, SeederService.reservations.length)
                ));
        });
        SeederService.sessions = await this.generate(_.random(1, SeederService.n), this.sessionRepository, async () => {
            return await this.fakeSession(
                _.sampleSize(SeederService.reservations, _.random(1, SeederService.reservations.length)), training, location
            );
        });
        SeederService.trainings = await this.generate(_.random(1, SeederService.n), this.trainingRepository, async () => {
            return await this.fakeTraining(
                _.sampleSize(SeederService.sessions, _.random(1, SeederService.sessions.length))
            );
        });
        SeederService.sessions = await this.generate(_.random(1, SeederService.n), this.sessionRepository, async () => {
            return await this.fakeSession(
                _.sampleSize(SeederService.reservations, _.random(1, SeederService.reservations.length)), training, location
            );
        });


        SeederService.locations = await this.generate(_.random(1, SeederService.n), this.locationRepository, async () => {
            return await this.fakeLocation(
                _.sampleSize(SeederService.sessions, _.random(1, SeederService.sessions.length)),
                [company]
            );
        });
        SeederService.companies = await this.generate(_.random(1, SeederService.n), this.companyRepository, async () => {
            return await this.fakeCompany(
                _.sampleSize(SeederService.locations, _.random(1, SeederService.locations.length))
            );
        });
        SeederService.locations = await this.generate(_.random(1, SeederService.n), this.locationRepository, async () => {
            return await this.fakeLocation(
                _.sampleSize(SeederService.sessions, _.random(1, SeederService.sessions.length)),
                _.sampleSize(SeederService.companies, _.random(1, SeederService.companies.length)),
            );
        });

        console.log("seeding has finished");
    }
}
