import {Module, OnApplicationBootstrap} from '@nestjs/common';
import {SeederService} from './seeder.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../models/User";
import {Company} from "../models/Company";
import {Location} from "../models/Location";
import {Reservation} from "../models/Reservation";
import {Session} from "../models/Session";
import {Training} from "../models/Training";
import {ModuleRef} from "@nestjs/core";
import {Image} from "../models/Image";
import {Address} from "../models/Address";
import {Coordinates} from "../models/Coordinates";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            User,
            Company,
            Location,
            Reservation,
            Session,
            Training,
            Image,
            Address,
            Coordinates
        ]),
        // AuthenticationService,
    ],
    providers: [SeederService],
    exports: [SeederService]
})
export class SeederModule implements OnApplicationBootstrap  {

    constructor(private moduleRef: ModuleRef) {}

    async onApplicationBootstrap() {
        //await this.moduleRef.get(SeederService).seed();
    }
}
