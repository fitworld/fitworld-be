import {BeforeInsert, Column, Entity, ManyToMany, ManyToOne, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {User, UserType} from "./User";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Session} from "./Session";
import {Reservation} from "./Reservation";
import uuid from "uuid/v4";
import shortid from "shortid";

export enum PaymentStatus {
    // Payments
    open = "open",
    canceled = "canceled",
    pending = "pending",
    authorized = "authorized",
    expired = "expired",
    failed = "failed",
    paid = "paid",

    // Refunds
    processing = "processing",
    queued = "queued",
    refund_pending = "refund_pending",
    refunded = "refunded",
}

export enum CurrencyCode {
    EUR = "EUR"
}

@Entity()
@ObjectType()
export class Payment extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => PaymentStatus)
    @Column({
        type: "enum",
        enum: PaymentStatus,
        default: PaymentStatus.open
    })
    status!: PaymentStatus;

    @Field(type => CurrencyCode)
    @Column({
        type: "enum",
        enum: CurrencyCode,
        default: CurrencyCode.EUR
    })
    currencyCode!: CurrencyCode;

    // @Field is missing explicitly here, since it could be exploited if mollie id is gotten
    @Column()
    mollieId!: string;

    @Field(type => String)
    @Column()
    amount!: string;

    @Field(type => String)
    @Column()
    description!: string;

    @Field(type => User)
    @ManyToOne(type => User, user => user.payments)
    user!: User;

    @Field(type => Reservation, {nullable: true})
    @ManyToOne(type => Reservation, reservation => reservation.payments, {nullable: true})
    reservation!: Reservation;

    @BeforeInsert()
    beforeInsert() {
        super.beforeInsert();
        this.description = `${this.description} | ${shortid.generate()}`;
    }
}
