import {Column, Entity, PrimaryColumn} from "typeorm";
import {Field, Float, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Location} from "./Location";

@Entity()
@ObjectType()
export class Coordinates extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => Float)
    @Column({type: "float"})
    latitude!: Number;

    @Field(type => Float)
    @Column({type: "float"})
    longitude!: Number;
}
