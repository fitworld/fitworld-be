import {Column, Entity, ManyToOne, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Training} from "./Training";

@Entity()
@ObjectType()
export class Image extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => String)
    @Column()
    url!: string;

    @Field(type => String, {nullable: true})
    @Column()
    alt!: string;

    @Field(type => Training, {nullable: true})
    @ManyToOne(type => Training, training => training.images, {nullable: true})
    training!: Training;

}
