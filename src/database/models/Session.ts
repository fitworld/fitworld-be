import {Column, Entity, ManyToOne, OneToMany, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Training} from "./Training";
import {Reservation} from "./Reservation";
import {Location} from "./Location";
import {Res} from "@nestjs/common";

@Entity()
@ObjectType()
export class Session extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => String)
    @Column()
    price!: string;

    @Field(type => Number)
    @Column()
    capacity!: number;

    @Field(type => Date)
    @Column("timestamp")
    time!: Date;

    @Field(type => Training)
    @ManyToOne(type => Training, training => training.sessions, {nullable: true})
    training!: Training;

    @Field(type => Location)
    @ManyToOne(type => Location, location => location.sessions, {nullable: true})
    location!: Location;

    @Field(type => [Reservation], {nullable: "items"})
    @OneToMany(type => Reservation, reservation => reservation.session, {nullable: true})
    reservations!: Reservation[]
}
