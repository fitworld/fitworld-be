import {Column, Entity, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";

@Entity()
@ObjectType()
export class Address extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => String)
    @Column()
    zipcode!: string;

    @Field(type => String)
    @Column()
    city!: string;

    @Field(type => String)
    @Column()
    address!: string;

    @Field(type => String)
    @Column()
    province!: string;
}
