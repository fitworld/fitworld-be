import {Column, Entity, OneToMany, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Session} from "./Session";
import {Image} from "./Image";

@Entity()
@ObjectType()
export class Training extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => String)
    @Column()
    name!: string;

    @Field(type => String)
    @Column({type: "text"})
    description!: string;

    @Field(type => Image, {nullable: "items"})
    @OneToMany(type => Image, image => image.training, {nullable: true})
    images!: Image[];

    @Field(type => [Session], {nullable: "items"})
    @OneToMany(type => Session, session => session.training, {nullable: true})
    sessions!: Session[];
}
