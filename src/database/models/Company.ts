import {Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToOne, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Location} from "./Location";
import {Image} from "./Image";
import {Address} from "./Address";

@Entity()
@ObjectType()
export class Company extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => String)
    @Column()
    name!: string;


    @Field(type => String)
    @Column()
    email!: string;

    @Field(type => String, {nullable: true})
    @Column()
    telephone_number!: string;

    @Field(type => String, {nullable: true})
    @Column()
    website!: string;

    @Field(type => [Location], {nullable: "items"})
    @ManyToMany(type => Location, location => location.companies, {nullable: true})
    @JoinTable()
    locations!: Location[];

    @Field(type => Image, {nullable: true})
    @OneToOne(type => Image, {nullable: true})
    @JoinColumn()
    logo!: Image;

    @Field(type => Address)
    @OneToOne(type => Address, {nullable: true})
    @JoinColumn()
    address!: Address;

}
