import {
    Column,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany, OneToMany,
    OneToOne,
    PrimaryColumn
} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Reservation} from "./Reservation";
import {Image} from "./Image";
import {Payment} from "./Payment";

export enum UserType {
    Anonymous = "Anonymous",
    User = "User",
}

export enum Role {
    None = -1,
    Anonymous,
    User,
    Trainer,
    // Admin,
    // Superadmin
}

@Entity()
@ObjectType()
export class User extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => String)
    @Column({default: ""})
    email!: string;

    @Field(type => String)
    @Column({default: ""})
    password!: string;

    @Field(type => Role)
    @Column({
        type: "enum",
        enum: Role,
        default: Role.Anonymous
    })
    role!: Role;

    @Field(type => UserType)
    @Column({
        type: "enum",
        enum: UserType,
        default: UserType.Anonymous
    })
    type!: UserType;


    @Field(type => Image, {nullable: true})
    @OneToOne(type => Image, {nullable: true})
    @JoinColumn()
    profileImage!: Image;

    @Field(type => [Payment], {nullable: true})
    @OneToMany(type => Payment, payment => payment.user, {nullable: true})
    @JoinColumn()
    payments!: Payment[];
}
