import {
    Column,
    Entity,
    JoinColumn,
    ManyToMany,
    OneToMany,
    OneToOne,
    PrimaryColumn
} from "typeorm";
import {Field, Float, ObjectType} from "type-graphql";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Session} from "./Session";
import {Company} from "./Company";
import {Address} from "./Address";
import {Coordinates} from "./Coordinates";

@Entity()
@ObjectType()
export class Location extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => Coordinates)
    @OneToOne(type => Coordinates, {nullable: true})
    @JoinColumn()
    coordinates!: Coordinates;

    @Field(type => Address)
    @OneToOne(type => Address, {nullable: true})
    @JoinColumn()
    address!: Address;

    @Field(type => [Address], {nullable: "items"})
    @OneToMany(type => Session, session => session.location, {nullable: true})
    sessions!: Session[];

    @Field(type => [Company], {nullable: "items"})
    @ManyToMany(type => Company, company => company.locations, {nullable: true})
    companies!: Company[];
}
