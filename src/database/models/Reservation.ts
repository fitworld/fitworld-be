import {Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn} from "typeorm";
import {Field, ObjectType} from "type-graphql";
import {User} from "./User";
import {OverrideBaseEntity} from "./OverrideBaseEntity";
import {Session} from "./Session";
import {Payment} from "./Payment";

@Entity()
@ObjectType()
export class Reservation extends OverrideBaseEntity {
    @Field(type => String)
    @PrimaryColumn()
    id!: string;

    @Field(type => Session)
    @ManyToOne(type => Session, session => session.reservations, {nullable: true})
    session!: Session;

    @Field(type => Payment, {nullable: "items"})
    @OneToMany(type => Payment, payment => payment.reservation, {nullable: true})
    @JoinTable()
    payments!: Payment[];

}
