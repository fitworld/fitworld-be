import {MigrationInterface, QueryRunner} from "typeorm";

export class Clean1584543252659 implements MigrationInterface {
    name = 'Clean1584543252659'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `training` (`createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `id` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `reservation` (`createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `id` varchar(255) NOT NULL, `sessionId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `location` (`createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `id` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `session` (`createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `id` varchar(255) NOT NULL, `trainingId` varchar(255) NULL, `locationId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `company` (`createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `id` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `user` (`createdAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `id` varchar(255) NOT NULL, `email` varchar(255) NOT NULL DEFAULT '', `password` varchar(255) NOT NULL DEFAULT '', `role` enum ('-1', '0', '1', '2') NOT NULL DEFAULT '0', `type` enum ('Anonymous', 'User') NOT NULL DEFAULT 'Anonymous', PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `company_locations_session` (`companyId` varchar(255) NOT NULL, `sessionId` varchar(255) NOT NULL, INDEX `IDX_a27d0c4f6ed136b0fcc497330c` (`companyId`), INDEX `IDX_0dc102ffb74130b8a79214a28e` (`sessionId`), PRIMARY KEY (`companyId`, `sessionId`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `user_reservations_reservation` (`userId` varchar(255) NOT NULL, `reservationId` varchar(255) NOT NULL, INDEX `IDX_31b66834ff1b8112abfabca36a` (`userId`), INDEX `IDX_3a4335c17da0d17311b2f96210` (`reservationId`), PRIMARY KEY (`userId`, `reservationId`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `reservation` ADD CONSTRAINT `FK_401a051b2814fbd09234f6329ee` FOREIGN KEY (`sessionId`) REFERENCES `session`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `session` ADD CONSTRAINT `FK_a6687a1939ff3f2ff034d6a1a73` FOREIGN KEY (`trainingId`) REFERENCES `training`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `session` ADD CONSTRAINT `FK_b1d3326c2ce67b7ec37489a9578` FOREIGN KEY (`locationId`) REFERENCES `location`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `company_locations_session` ADD CONSTRAINT `FK_a27d0c4f6ed136b0fcc497330c3` FOREIGN KEY (`companyId`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `company_locations_session` ADD CONSTRAINT `FK_0dc102ffb74130b8a79214a28e9` FOREIGN KEY (`sessionId`) REFERENCES `session`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `user_reservations_reservation` ADD CONSTRAINT `FK_31b66834ff1b8112abfabca36ad` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `user_reservations_reservation` ADD CONSTRAINT `FK_3a4335c17da0d17311b2f962104` FOREIGN KEY (`reservationId`) REFERENCES `reservation`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_reservations_reservation` DROP FOREIGN KEY `FK_3a4335c17da0d17311b2f962104`", undefined);
        await queryRunner.query("ALTER TABLE `user_reservations_reservation` DROP FOREIGN KEY `FK_31b66834ff1b8112abfabca36ad`", undefined);
        await queryRunner.query("ALTER TABLE `company_locations_session` DROP FOREIGN KEY `FK_0dc102ffb74130b8a79214a28e9`", undefined);
        await queryRunner.query("ALTER TABLE `company_locations_session` DROP FOREIGN KEY `FK_a27d0c4f6ed136b0fcc497330c3`", undefined);
        await queryRunner.query("ALTER TABLE `session` DROP FOREIGN KEY `FK_b1d3326c2ce67b7ec37489a9578`", undefined);
        await queryRunner.query("ALTER TABLE `session` DROP FOREIGN KEY `FK_a6687a1939ff3f2ff034d6a1a73`", undefined);
        await queryRunner.query("ALTER TABLE `reservation` DROP FOREIGN KEY `FK_401a051b2814fbd09234f6329ee`", undefined);
        await queryRunner.query("DROP INDEX `IDX_3a4335c17da0d17311b2f96210` ON `user_reservations_reservation`", undefined);
        await queryRunner.query("DROP INDEX `IDX_31b66834ff1b8112abfabca36a` ON `user_reservations_reservation`", undefined);
        await queryRunner.query("DROP TABLE `user_reservations_reservation`", undefined);
        await queryRunner.query("DROP INDEX `IDX_0dc102ffb74130b8a79214a28e` ON `company_locations_session`", undefined);
        await queryRunner.query("DROP INDEX `IDX_a27d0c4f6ed136b0fcc497330c` ON `company_locations_session`", undefined);
        await queryRunner.query("DROP TABLE `company_locations_session`", undefined);
        await queryRunner.query("DROP TABLE `user`", undefined);
        await queryRunner.query("DROP TABLE `company`", undefined);
        await queryRunner.query("DROP TABLE `session`", undefined);
        await queryRunner.query("DROP TABLE `location`", undefined);
        await queryRunner.query("DROP TABLE `reservation`", undefined);
        await queryRunner.query("DROP TABLE `training`", undefined);
    }

}
