import {ExpressAdapter} from "@nestjs/platform-express";
import {NestFactory, Reflector} from "@nestjs/core";
import {CoreModule} from "./core/core.module";
import * as core from "express-serve-static-core";
import rateLimit from "express-rate-limit";
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import {INestApplication, ValidationPipe} from "@nestjs/common";
import registerEnums from "./graphql/registerEnums";
import isDev from "./util/isDev";
import {RootGuard} from "./authentication/guards/root.guard";
import {AuthenticationService} from "./authentication/authentication.service";
import {RedisService} from "./redis/redis.service";
import {SeederService} from "./database/seeder/seeder.service";

const setupSwagger = async (app: INestApplication) => {
    const options = new DocumentBuilder()
        .setTitle('Fitworld BE')
        .setDescription("Fitworld's main BE")
        .setVersion('0.0.1')
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('swagger', app, document);
};

const setupGraphQLType = async (app: INestApplication) => {
    // Register enums in GraphQL before nest starts
    registerEnums();
};

const setupMisc = async (app: INestApplication) => {
    app.enableCors({
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
        preflightContinue: false,
        optionsSuccessStatus: 204,
        allowedHeaders: "*",
        exposedHeaders: "Authorization,Content-Length,Content-Type"
    });
    app.useGlobalPipes(new ValidationPipe());
    app.useGlobalGuards(new RootGuard(
        app.get<AuthenticationService>(AuthenticationService),
        app.get<RedisService>(RedisService),
        app.get<Reflector>(Reflector),
    ));
    // 300 requests in 3 min is max, then we start rate-limiting (very generous)
    app.use(
        rateLimit({
            windowMs: 3 * 60 * 1000,
            max: 300
        }),
    );
};

const runSeed = async (app: INestApplication) => {
    const seeder = app.get(SeederService);
    await seeder.seed();
};

export default async (expressApp: core.Express) => {
    const adapter = new ExpressAdapter(expressApp);
    const app = await NestFactory.create(CoreModule, adapter);

    // Setup steps, divided in contexts
    if (isDev()) await setupSwagger(app);
    await setupGraphQLType(app);
    await setupMisc(app);
    await app.init();

    // await runSeed(app);
    return app;
}
